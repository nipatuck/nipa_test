package com.nipa.testscb.model;

/**
 * Created by thekhaeng on 3/20/2017 AD.
 */

public class MobileURL {

     public static final String URL_PRODUCT= "api/mobiles/";
     public static final String URL_IMAGE= "api/mobiles/{id}/images/";
    public static final String BASE_URL_TEST = "https://scb-test-mobile.herokuapp.com/";

}
