package com.nipa.testscb.detailproduct;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;


import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.nipa.favoritelist.R;
import com.nipa.testscb.product.ProductPresenter;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DetailProductActivity extends AppCompatActivity implements DetailProductContract.ProductDetailView {

    private static final String HI = "https://scb-test-mobile.herokuapp.com/api/mobiles/";
    Toolbar toolbar;


    CoordinatorLayout rootLayout;
    String id_product, name_product, brand_product, description_product;
    Bundle m_bundle;
    private JsonArrayRequest request;
    private RequestQueue requestQueue;
    ArrayList<String> list_img;
    HashMap<String, String> mapBanner;

    TextView txt_name, txt_brand, txt_description;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    DetailProductPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        initToolbar();
        initView();
        initInstances();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("MobilePhone");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }
    }

    private void initView() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_brand = (TextView) findViewById(R.id.txt_brand);
        txt_description = (TextView) findViewById(R.id.txt_description);


    }

    private void initInstances() {


        rootLayout = (CoordinatorLayout) findViewById(R.id.rootLayout);

        m_bundle = getIntent().getExtras();
        if (m_bundle != null) {
            id_product = m_bundle.getString("id_product");
            name_product = m_bundle.getString("name_product");
            brand_product = m_bundle.getString("brand_product");
            description_product = m_bundle.getString("description_product");
        }

        txt_name.setText(name_product);
        txt_brand.setText(brand_product);
        txt_description.setText(description_product);


        presenter = new DetailProductPresenter(this, id_product);
        presenter.getDetailProducts();

    }


    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    public void setAdapter(ArrayList<String> listImage) {
        viewPagerAdapter = new ViewPagerAdapter(this, listImage);
        viewPager.setAdapter(viewPagerAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Context getContext() {
        getApplication();
        return getApplication();
    }


    @Override
    public void showDetailProducts(ArrayList<String> detail) {
        list_img = detail;
        setAdapter(list_img);

    }

    @Override
    public void showImage(String urlImg) {
        list_img.add(urlImg);
        setAdapter(list_img);
    }
}

