package com.nipa.testscb.product;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;
import com.nipa.testscb.favorite.FavoriteDatabase;
import com.nipa.testscb.favorite.FavoriteFragment;
import com.nipa.favoritelist.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.nipa.testscb.model.MobileURL.BASE_URL_TEST;
import static com.nipa.testscb.model.MobileURL.URL_PRODUCT;


public class MainActivity extends AppCompatActivity implements ProductContract.ProductView {

    private List<Product_List> product_lists;
    MainTabAdapter tabAdapter;

    private JsonArrayRequest request;
    private RequestQueue requestQueue;
    public static FavoriteDatabase favoriteDatabase;
    private ProgressBar pgsBar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ProductContract.ProductPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle("MobilePhone");


        pgsBar = (ProgressBar) findViewById(R.id.pBar);

        product_lists = new ArrayList<>();


        viewPager = (ViewPager) findViewById(R.id.tabanim_viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        tabLayout.addTab(tabLayout.newTab().setText("mobile list"));
        tabLayout.addTab(tabLayout.newTab().setText("favorite list"));

        favoriteDatabase = Room.databaseBuilder(getApplicationContext(), FavoriteDatabase.class, "myfavdb").allowMainThreadQueries().build();


        presenter = new ProductPresenter(this);
        presenter.getAllProducts();

    }


    public void setAdapter(List<Product_List> product_lists) {
        tabAdapter = new MainTabAdapter(getApplicationContext(), getSupportFragmentManager(), tabLayout.getTabCount(), product_lists);
        viewPager.setAdapter(tabAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 2) {
                    FavoriteFragment favoriteFragment = (FavoriteFragment) tabAdapter.instantiateItem(viewPager, tab.getPosition());
                    favoriteFragment.update();

                }


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void showLoading() {
        pgsBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pgsBar.setVisibility(View.GONE);
    }

    @Override
    public Context getContext() {
        getApplication();
        return getApplication();
    }


    @Override
    public void showAllProducts(List<Product_List> product) {
        product_lists = product;
        setAdapter(product_lists);
    }
}
