package com.nipa.testscb.detailproduct;


import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.nipa.testscb.model.ApiInterface;
import com.nipa.testscb.model.Apis;
import com.nipa.testscb.model.Details;
import com.nipa.testscb.product.ProductContract;
import com.nipa.testscb.product.Product_List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nipa.testscb.model.MobileURL.BASE_URL_TEST;


public class DetailProductPresenter implements DetailProductContract.ProductDetailPresenter {
    private DetailProductContract.ProductDetailView view;
    private ApiInterface apiInterface;

    private String id;
    ArrayList<String> list_img;
    private JsonArrayRequest request;
    private RequestQueue requestQueue;
    private Context context;

    public DetailProductPresenter(DetailProductContract.ProductDetailView view, String id) {
        this.view = view;
        this.id = id;
        apiInterface = Apis.getProductApi();

    }

    @Override
    public void getDetailProducts() {

        getData();

        list_img = new ArrayList<>();
        apiInterface.getDetail(id).enqueue(new Callback<Details>() {
            @Override
            public void onResponse(Call<Details> call, Response<Details> response) {
                Details details = response.body();

                list_img.add(details.url);

                view.showImage(details.url);

            }

            @Override
            public void onFailure(Call<Details> call, Throwable t) {

            }

        });

//        view.showDetailProducts(list_img);
    }


    private void getData() {
        String api_detail = BASE_URL_TEST+ "api/mobiles/" +id +"/images/";
        request = new JsonArrayRequest(api_detail, new com.android.volley.Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                list_img = new ArrayList<String>();
                for (int i = 0; i < response.length(); i++) {
                    try {

                        JSONObject obj = response.getJSONObject(i);
                        String url_img = obj.getString("url");

                        list_img.add(url_img);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                view.showDetailProducts(list_img);

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        context = view.getContext();
        requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }
}

