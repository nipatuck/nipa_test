package com.nipa.testscb.product;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nipa.testscb.detailproduct.DetailProductActivity;
import com.nipa.testscb.favorite.FavoriteList;
import com.nipa.favoritelist.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    List<Product_List> product_lists;
    Context ct;


    public ProductAdapter(List<Product_List> product_lists, Context ct) {
        this.product_lists = product_lists;
        this.ct = ct;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final Product_List productList = product_lists.get(i);
        String pimg = productList.getThumbImageURL();

        Picasso.with(ct).load(pimg).into(viewHolder.img);

        String price = String.valueOf(productList.getPrice());
        String rating = String.valueOf(productList.getRating());

        viewHolder.tv.setText(productList.getName());
        viewHolder.tv_description.setText(productList.getDescription());
        viewHolder.tv_price.setText("Price: $"+ price);
        viewHolder.tv_rating.setText("Rating: "+ rating);


        if (MainActivity.favoriteDatabase.favoriteDao().isFavorite(productList.getId()) == 1)
            viewHolder.fav_btn.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        else
            viewHolder.fav_btn.setImageResource(R.drawable.ic_favorite);


        viewHolder.fav_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                FavoriteList favoriteList = new FavoriteList();

                int id = productList.getId();
                String image = productList.getThumbImageURL();
                String name = productList.getName();
                double price = productList.getPrice();


                favoriteList.setId(id);
                favoriteList.setThumbImageURL(image);
                favoriteList.setName(name);
                favoriteList.setPrice(price);

                if (MainActivity.favoriteDatabase.favoriteDao().isFavorite(id) != 1) {
//                    viewHolder.fav_btn.setImageResource(R.drawable.ic_favorite);
                    viewHolder.fav_btn.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                    MainActivity.favoriteDatabase.favoriteDao().addData(favoriteList);


                } else {
                    viewHolder.fav_btn.setImageResource(R.drawable.ic_favorite);
//                    viewHolder.fav_btn.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                    MainActivity.favoriteDatabase.favoriteDao().delete(favoriteList);


                }


            }
        });

        viewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(ct, "id_product :"+ productList.getId(),
//                        Toast.LENGTH_LONG).show();
                String id_product = String.valueOf(productList.getId());

                Intent i = new Intent(ct, DetailProductActivity.class);
                i.putExtra("id_product",id_product);
                i.putExtra("name_product",productList.getName());
                i.putExtra("brand_product",productList.getBrand());
                i.putExtra("description_product",productList.getDescription());
                ct.startActivity(i);
            }
        });
    }


    @Override
    public int getItemCount() {
        return product_lists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img, fav_btn;
        TextView tv,tv_description,tv_price,tv_rating;
        CardView card_view;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img_pr);
            tv = (TextView) itemView.findViewById(R.id.tv_name);
            tv_description = (TextView) itemView.findViewById(R.id.tv_description);
            tv_price = (TextView) itemView.findViewById(R.id.tv_price);
            tv_rating = (TextView) itemView.findViewById(R.id.tv_rating);
            fav_btn = (ImageView) itemView.findViewById(R.id.fav_btn);
            card_view = (CardView) itemView.findViewById(R.id.card_view);

        }
    }
}
