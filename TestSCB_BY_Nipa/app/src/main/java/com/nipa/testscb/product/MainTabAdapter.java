package com.nipa.testscb.product;


import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import com.nipa.testscb.favorite.FavoriteFragment;

import java.util.List;



public class MainTabAdapter extends FragmentPagerAdapter {

    private List<Product_List> product_lists;
    private Context context;
    int totalTabs;

    public MainTabAdapter(Context context, FragmentManager fm, int totalTabs, List<Product_List> product_lists) {
        super(fm);
        this.product_lists = product_lists;
        this.context = context;
        this.totalTabs = totalTabs;

    }

    @Override
    public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    ProductFragment tab1 = new ProductFragment(product_lists);
                    return tab1;
                case 1:
                    FavoriteFragment tab2 = new FavoriteFragment();
                    return tab2;
                default:
                    return null;
            }

    }

    @Override
    public int getCount() {
        return totalTabs;
    }

}
