package com.nipa.testscb.product;


import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.nipa.testscb.model.ApiInterface;
import com.nipa.testscb.model.Apis;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.nipa.testscb.model.MobileURL.BASE_URL_TEST;
import static com.nipa.testscb.model.MobileURL.URL_PRODUCT;

public class ProductPresenter implements ProductContract.ProductPresenter {
    private ProductContract.ProductView view;
    private ApiInterface apiInterface;
    private RequestQueue requestQueue;
    private JsonArrayRequest request;
    private List<Product_List> product_lists;
    private Context context;

    public ProductPresenter(ProductContract.ProductView view) {
        this.view = view;
        apiInterface = Apis.getProductApi();

    }

    @Override
    public void getAllProducts() {
        view.showLoading();
        product_lists = new ArrayList<>();
        request = new JsonArrayRequest(BASE_URL_TEST + URL_PRODUCT, new com.android.volley.Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                view.hideLoading();

                for (int i = 0; i < response.length(); i++) {
                    try {

                        JSONObject ob = response.getJSONObject(i);
                        Product_List pr = new Product_List(
                                ob.getString("name"),
                                ob.getString("description"),
                                ob.getInt("id"),
                                ob.getString("thumbImageURL"),
                                ob.getDouble("rating"),
                                ob.getDouble("price"),
                                ob.getString("brand"));
                        product_lists.add(pr);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                view.showAllProducts(product_lists);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        context = view.getContext();
        requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }


}

