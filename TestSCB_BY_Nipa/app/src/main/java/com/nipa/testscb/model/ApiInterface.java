package com.nipa.testscb.model;

import com.nipa.testscb.product.Product_List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import static com.nipa.testscb.model.MobileURL.URL_IMAGE;
import static com.nipa.testscb.model.MobileURL.URL_PRODUCT;

public interface ApiInterface {
    @GET(URL_PRODUCT)
    Call<Product_List> getAllProduct();

    @GET(URL_IMAGE)
    Call<Details> getDetail(@Path("id") String id);
}
