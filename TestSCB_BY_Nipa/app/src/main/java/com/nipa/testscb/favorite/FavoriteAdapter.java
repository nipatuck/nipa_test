package com.nipa.testscb.favorite;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.nipa.favoritelist.R;
import com.nipa.testscb.detailproduct.DetailProductActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {
    private List<FavoriteList> favoriteLists;
    Context context;

    public FavoriteAdapter(List<FavoriteList> favoriteLists, Context context) {
        this.favoriteLists = favoriteLists;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.favourites_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final FavoriteList fl = favoriteLists.get(i);
        Picasso.with(context).load(fl.getThumbImageURL()).into(viewHolder.img);
        String price = String.valueOf(fl.getPrice());
        String rating = String.valueOf(fl.getRating());
        viewHolder.fav_txt_name.setText(fl.getName());
        viewHolder.fav_txt_price.setText(price);
        viewHolder.fav_txt_rating.setText("Rating : "+rating);




        viewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(ct, "id_product :"+ productList.getId(),
//                        Toast.LENGTH_LONG).show();
                String id_product = String.valueOf(fl.getId());

                Intent i = new Intent(context, DetailProductActivity.class);
                i.putExtra("id_product",id_product);
                i.putExtra("name_product",fl.getName());
                i.putExtra("brand_product",fl.getBrand());
                i.putExtra("description_product",fl.getDescription());
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return favoriteLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView fav_txt_name,fav_txt_price,fav_txt_rating;
        CardView card_view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.fav_img);
            fav_txt_name = (TextView) itemView.findViewById(R.id.fav_txt_name);
            fav_txt_price = (TextView) itemView.findViewById(R.id.fav_txt_price);
            fav_txt_rating = (TextView) itemView.findViewById(R.id.fav_txt_rating);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
        }
    }
}
