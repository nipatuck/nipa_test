package com.nipa.testscb.detailproduct;


import android.content.Context;

import com.nipa.testscb.product.Product_List;

import java.util.ArrayList;
import java.util.List;

public interface DetailProductContract {
    interface ProductDetailView {


        void showDetailProducts(ArrayList<String> detail);

        void showImage(String urlImg);

        Context getContext();


    }

    interface ProductDetailPresenter {
        void getDetailProducts();

    }
}
