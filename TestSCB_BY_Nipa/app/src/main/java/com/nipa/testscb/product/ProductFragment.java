package com.nipa.testscb.product;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.nipa.favoritelist.R;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ProductFragment extends Fragment {

    List<Product_List> product_lists;
    ProductAdapter adapter;
    private RecyclerView recyclerView;
    int checkedItem = 0;
    int checkCurrentItem = 0;


    public ProductFragment(List<Product_List> product_lists) {
        this.product_lists = product_lists;
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_product, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        adapter = new ProductAdapter(product_lists, getContext());
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sort, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.menu_sort:
                dialogSort();

                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return false;


    }



    public void dialogSort(){
        final String[] listItems = {"Price low to high", "Price high to low", "Rating"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Sort");

        checkedItem = checkCurrentItem; //this will checked the item when user open the dialog
        builder.setSingleChoiceItems(listItems, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                checkCurrentItem = position;

                switch (position) {
                    case 0:

                        Collections.sort(product_lists, new Comparator<Product_List>() {
                            @Override
                            public int compare(Product_List o1, Product_List o2) {
                                return Double.compare(o1.getPrice(), o2.getPrice());
                            }
                        });


//                        adapter.notifyDataSetChanged();
//                        dialog.dismiss();

                        break;
                    case 1:

                        Collections.sort(product_lists, new Comparator<Product_List>() {
                            @Override
                            public int compare(Product_List o1, Product_List o2) {
                                return Double.compare(o2.getPrice(), o1.getPrice());
                            }
                        });

//                        adapter.notifyDataSetChanged();
//                        dialog.dismiss();

                        break;

                    case 2:
                        Collections.sort(product_lists, new Comparator<Product_List>() {
                            @Override
                            public int compare(Product_List o1, Product_List o2) {
                                return Double.compare(o2.getRating(), o1.getRating());
                            }
                        });

//                        adapter.notifyDataSetChanged();
//                        dialog.dismiss();
                        break;

                }
            }
        });


        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();


        final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        positiveButtonLL.gravity = Gravity.CENTER;
        positiveButton.setLayoutParams(positiveButtonLL);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



}
