package com.nipa.testscb.favorite;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.nipa.favoritelist.R;
import com.nipa.testscb.product.MainActivity;
import com.nipa.testscb.product.Product_List;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class FavoriteFragment extends Fragment {

    private RecyclerView rv;
    private FavoriteAdapter adapter;
    int checkedItem = 0;
    int checkCurrentItem = 0;
    List<FavoriteList> favoriteLists;
    private ArrayList<HashMap<String, String>> favList = new ArrayList<>();


    public FavoriteFragment() {
        // Required empty public constructor

    }
    public static FavoriteFragment newInstance(List<FavoriteList> favoriteLists) {
        FavoriteFragment f = new FavoriteFragment();
        Bundle args = new Bundle();

        f.setArguments(args);
        return f;
    }



    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_favorite, container, false);
        rv = (RecyclerView) v.findViewById(R.id.rec_favorite);
        rv.setHasFixedSize(true);
        favoriteLists = MainActivity.favoriteDatabase.favoriteDao().getFavoriteData();
        adapter = new FavoriteAdapter(favoriteLists, getContext());
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llm);
        rv.setAdapter(adapter);

//        rv.setLayoutManager(new LinearLayoutManager(getContext()));

//        getFavData();

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sort, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.menu_sort:
                dialogSort();

                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return false;


    }

    public void update() {
//        favoriteLists = MainActivity.favoriteDatabase.favoriteDao().getFavoriteData();
//        adapter = new FavoriteAdapter(favoriteLists, getContext());
//        adapter.notifyDataSetChanged();

        Fragment frg = null;
        frg = getActivity().getSupportFragmentManager().findFragmentByTag("Your_Fragment_TAG");
        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.detach(frg);
        ft.attach(frg);
        ft.commit();
        }

    public void dialogSort() {
        final String[] listItems = {"Price low to high", "Price high to low", "Rating"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Sort");

        checkedItem = checkCurrentItem; //this will checked the item when user open the dialog
        builder.setSingleChoiceItems(listItems, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                checkCurrentItem = position;

                switch (position) {
                    case 0:

                        Collections.sort(favoriteLists, new Comparator<FavoriteList>() {
                            @Override
                            public int compare(FavoriteList o1, FavoriteList o2) {
                                return Double.compare(o1.getPrice(), o2.getPrice());
                            }
                        });


                        break;
                    case 1:

                        Collections.sort(favoriteLists, new Comparator<FavoriteList>() {
                            @Override
                            public int compare(FavoriteList o1, FavoriteList o2) {
                                return Double.compare(o2.getPrice(), o1.getPrice());
                            }
                        });


                        break;

                    case 2:
                        Collections.sort(favoriteLists, new Comparator<FavoriteList>() {
                            @Override
                            public int compare(FavoriteList o1, FavoriteList o2) {
                                return Double.compare(o2.getRating(), o1.getRating());
                            }
                        });

//                        adapter.notifyDataSetChanged();
//                        dialog.dismiss();
                        break;

                }
            }
        });


        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();


        final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        positiveButtonLL.gravity = Gravity.CENTER;
        positiveButton.setLayoutParams(positiveButtonLL);
    }


}
