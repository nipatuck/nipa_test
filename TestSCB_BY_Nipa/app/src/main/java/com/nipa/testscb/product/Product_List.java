package com.nipa.testscb.product;

//          name: "Moto E4 Plus",
//        description: "First place in our list goes to the excellent Moto E4 Plus. It's a cheap phone that features phenomenal battery life, a fingerprint scanner and a premium feel design, plus it's a lot cheaper than the Moto G5 below. It is a little limited with its power, but it makes up for it by being able to last for a whole two days from a single charge. If price and battery are the most important features for you, the Moto E4 Plus will suit you perfectly.",
//        id_product: 1,
//        thumbImageURL: "https://cdn.mos.cms.futurecdn.net/grwJkAGWQp4EPpWA3ys3YC-650-80.jpg",
//        rating: 4.9,
//        price: 179.99,
//        brand: "Samsung",

public class Product_List {

    private String name;
    private String description;
    private int id;
    private String thumbImageURL;
    private double rating;
    private double price;
    private String brand;



    public Product_List(String name, String description,int id,String thumbImageURL,double rating,double price ,String brand) {
        this.name = name;
        this.description = description;
        this.id =id;
        this.thumbImageURL = thumbImageURL;
        this.rating = rating;
        this.price = price;
        this.brand = brand;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThumbImageURL() {
        return thumbImageURL;
    }

    public void setThumbImageURL(String thumbImageURL) {
        this.thumbImageURL = thumbImageURL;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

}