package com.nipa.testscb.product;


import android.content.Context;

import java.util.List;

public interface ProductContract {
    interface ProductView {

        void showLoading();

        void hideLoading();

        Context getContext();

        void showAllProducts(List<Product_List> product);


    }

    interface ProductPresenter {
        void getAllProducts();

    }
}
